# Resumes API

![GitHub last commit](https://img.shields.io/github/last-commit/victorstein/resumeAPI.svg?style=for-the-badge)

This is a Firebase hosted graphql API that will simplify the process of hosting resume information and consume it through an API.

You may try the API playground thorugh this [Link](https://us-central1-resumeapi-b4e7d.cloudfunctions.net/api/graphql)

### Coding style

![GitHub last commit](https://img.shields.io/badge/STYLE-JAVASCRIPT%20STANDARD-yellow.svg?style=for-the-badge&logo=javascript)

JavaScript Standard

## Built With

* [express](https://expressjs.com/en/4x/api.html) - The web framework used
* [apollo-server-express](https://github.com/apollographql/apollo-server/tree/master/packages/apollo-server-express) - The Express integration of GraphQL Server
* [graphql](https://github.com/graphql/graphql-js) - The JavaScript reference implementation for GraphQL

## Contributing

Feel free to submit your PRs for review. There's currently no template for contribution. As the project grows we will look into further implementation of this.

## Authors

<!-- prettier-ignore -->
<table><tr><td align="center"><a href="http://victorstein.github.io"><img src="https://avatars3.githubusercontent.com/u/11080740?v=3" width="100px;" alt="Kent C. Dodds"/><br /><sub><b>Alfonso Gomez</b></sub></a><br /><a href="#question-kentcdodds" title="Answering Questions">💬</a> <a href="https://github.com/all-contributors/all-contributors/commits?author=kentcdodds" title="Documentation">📖</a><a href="#tool-jfmengels" title="Tools">🔧</a> <a href="#review-kentcdodds" title="Reviewed Pull Requests">👀</a> <a href="#infra-jakebolam" title="Infrastructure (Hosting, Build-Tools, etc)">🚇</a> <a href="#maintenance-jakebolam" title="Maintenance">🚧</a></td><td align="center"><a href="https://github.com/GustavoGomez092"><img src="https://avatars0.githubusercontent.com/u/45272016?v=3" width="100px;" alt="Jeroen Engels"/><br /><sub><b>Gustavo Gomez</b></sub></a><br /><a href="https://github.com/all-contributors/all-contributors/commits?author=jfmengels" title="Documentation">📖</a> <a href="#tool-jfmengels" title="Tools">🔧</a><a href="#maintenance-jakebolam" title="Maintenance">🚧</a></td><td align="center"><a href="https://github.com/Mes1991"><img src="https://avatars2.githubusercontent.com/u/51966183?v=4" width="100px;" alt="Jake Bolam"/><br /><sub><b>Marcos Espinal</b></sub></a><br /><a href="https://github.com/all-contributors/all-contributors/commits?author=jakebolam" title="Documentation">📖</a> <a href="#tool-jakebolam" title="Tools">🔧</a> <a href="#maintenance-jakebolam" title="Maintenance">🚧</a></td></table>

## License

This project is licensed under the MIT License 