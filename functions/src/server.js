import * as functions from 'firebase-functions'
import express from 'express'
import { ApolloServer } from 'apollo-server-express'
import resolvers from './resolvers'
import typeDefs from './typeDefs'

const app = express()

const server = new ApolloServer({
  resolvers,
  typeDefs,
  playground: true
})

server.applyMiddleware({ app })

exports.api = functions.https.onRequest(app)
