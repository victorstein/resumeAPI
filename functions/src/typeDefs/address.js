import { gql } from 'apollo-server-express'

export default gql`
 type Address {
   address1: String
   address2: String
   city: String
   country: String
 }
`
