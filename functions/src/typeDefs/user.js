import { gql } from 'apollo-server-express'

export default gql`
  type User {
    firstName: String!
    lastName: String
    email: String!
    password: String!
    apiKey: String
    userPic: String
    resumes: [Resume]
    personalInfo: PersonalInfo
    addresses: [Address]
  }

  extend type Query {
    user: User
  }
`
