import { gql } from 'apollo-server-express'

export default gql`
  type PersonalInfo {
    dob: String
    cedula: String
    mobilePhone: [String]
    landLine: [String]
    socialNetworks: [Social]
  }
`
