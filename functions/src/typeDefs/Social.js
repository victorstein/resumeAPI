import { gql } from 'apollo-server-express'

export default gql`
 type Social {
   socialNetwork: String
   url: String
 }
`
