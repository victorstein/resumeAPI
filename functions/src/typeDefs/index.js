import root from './root'
import user from './user'
import resume from './resume'
import personalInfo from './personalInfo'
import address from './address'
import social from './Social'
import education from './education'
import skill from './skill'
import professionalHistory from './professionalhistory'
import skillType from './skilltype'
import portfolio from './portfolio'
import technology from './technology'

export default [
  root,
  user,
  resume,
  personalInfo,
  address,
  social,
  education,
  skill,
  professionalHistory,
  skillType,
  portfolio,
  technology
]
