import { gql } from 'apollo-server-express'

export default gql`
 type Technology {
   name: String
   proficiency: Int
 }
`
