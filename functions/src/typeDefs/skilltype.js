import { gql } from 'apollo-server-express'

export default gql`
 type SkillType {
   social: Boolean!
   technical: Boolean!
 }
`
