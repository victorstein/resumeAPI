import { gql } from 'apollo-server-express'

export default gql`
 type Skill {
   name: String
   description: String
   percentage: Int
   skillType: SkillType
 }
`
