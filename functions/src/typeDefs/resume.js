import { gql } from 'apollo-server-express'

export default gql`
  type Resume {
    title: String
    personalInfo: PersonalInfo
    education: [Education]
    skills: [Skill]
    technologies: [Technology]
    portfolio: [Portfolio]
  }
`
