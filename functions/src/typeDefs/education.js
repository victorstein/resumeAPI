import { gql } from 'apollo-server-express'

export default gql`
 type Education {
   dateFrom: String
   dateTo: String
   description: String
   schoolName: String
   titleObtained: String
 }
`
