import { gql } from 'apollo-server-express'

export default gql`
 type Portfolio {
   name: String
   technologie: [String!]
   platform: String!
   imageUrl: String
 }
`
