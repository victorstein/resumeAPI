import { gql } from 'apollo-server-express'

export default gql`
 type Professionalhistory {
   workplace: String
   dateFrom: String
   dateTo: String
   description: String
   position: String
 }
`
