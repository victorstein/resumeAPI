import { ApolloError } from 'apollo-server-express'

export default {
  Query: {
    user: (root, args, context, info) => {
      try {
        return {
          firstName: 'lalo'
        }
      } catch (e) {
        throw new ApolloError(e)
      }
    }
  },
  Mutation: {

  }
}
